<?php
$faculty = array("MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu");
$facultyName = $keyWord = '';
if (isset($_GET['faculty'])) {
    $facultyName = $_GET['faculty'];
}
if (isset($_GET['keyword'])) {
    $keyWord = $_GET['keyword'];
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>List student</title>
    <style>
    body {
        font-family: sans-serif;
        padding: 10px;
    }

    * {
        margin: 0;
        padding: 0;
        box-sizing: border-box;
    }

    .search-form {
        display: flex;
        flex-direction: column;
        width: 40%;
        padding: 10px 30px;
    }

    select {
        border: solid 2px #007bc7;
        outline: none;
        width: 100%;
        padding: 8px;
    }

    table {
        width: 100%;
        border-collapse: separate;
        border-spacing: 0 1em;
    }

    td,
    th {
        text-align: left;
        padding: 8px;
    }

    form td {
        text-align: center;
    }

    .wrap-action {
        width: 88%;
        display: flex;
        justify-content: end;
    }

    .action {
        color: white;
        display: inline-block;
        padding: 10px;
        background-color: 	#0096FF;
        border: 1px solid #007bc7;
        text-decoration: none;
        border-radius: 10px;
        margin-right: 8px;
    }
    </style>

    <script>
    document.addEventListener('DOMContentLoaded', (event) => {
        const btnClear = document.getElementById('btn-clear');
        const selectSpec = document.getElementById('faculty');
        const inputKeyWord = document.getElementById('key');
        btnClear.addEventListener('click', (e) => {
            console.log(inputKeyWord.value + ',' + selectSpec.value)
            selectSpec.value = ''
            inputKeyWord.value = ''
            console.log(inputKeyWord.value + ',' + selectSpec.value)
            const urlObj = new URL(location.href);
            urlObj.searchParams.delete(selectSpec.name);
            urlObj.searchParams.delete(inputKeyWord.name)
            window.history.pushState({}, '', urlObj.toString())
        })
    })
    </script>    
</head>

<body>
    <div class="search-form">
        <form name="registerForm" method="GET" enctype="multipart/form-data" action="">
            <table>
            <tr height = '40px'>
                <td style = 'vertical-align: top; text-align: left; padding: 15px 15px'>
                    <label style='color: black;'>Khoa</label>
                </td>
                <td height = '40px'>
                    <select id="faculty" name='faculty' style = 'border-color:	#0096FF;
                                                    height: 100%; width: 80%;'>
                            <option disabled value <?= !$facultyName ? 'selected' : '' ?>></option>
                            <?php foreach ($faculty as $key => $value) : ?>
                            <option value="<?= $key ?>" <?= $facultyName === $key ? 'selected' : '' ?>><?= $value ?>
                            </option>
                            <?php endforeach; ?>
                    </select>
                </td>
            </tr>
            <tr height = '40px'>
            <td style = 'vertical-align: top; text-align: left; padding: 15px 15px'>
                    <label style='color: black;'>Từ khoá</label>
                </td>
                <td height = '40px'>
                    <input id="key" name="keyword" type="text" value="<?= htmlspecialchars($keyWord) ?>" style = 'border-color:	#0096FF;
                                                    height: 100%; width: 80%;'>
                </td>
            </tr>
            </table>
            <tr>
                <td>                                      
                <button style='background-color: 	#0096FF; border-radius: 10px; 
                    width: 100px; height: 43px; border-width: 0; margin: 15px 200px; color: white;'>Tìm kiếm</button>
                </td>
            </tr>            
        </form>
    </div>
    <div class="result-search">
        <span>Số sinh viên tìm thấy:</span>
        <span>XXX</span>
    </div>
    <div class="list-student">
        <div class="wrap-action">
            <a class="action" href="./web_register.php">Thêm</a>
        </div>
        <div class="table-student">
            <table>
                <colgroup>
                    <col span="1" style="width: 10%;">
                    <col span="1" style="width: 25%;">
                    <col span="1" style="width: 50%;">
                    <col span="1" style="width: 15%;">
                </colgroup>
                <tr>
                    <th>No</th>
                    <th>Tên sinh viên</th>
                    <th>Khoa</th>
                    <th>Action</th>
                </tr>
                <tr>
                    <td>1</td>
                    <td>Nguyễn Văn A</td>
                    <td>Khoa học máy tính</td>
                    <td>
                        <a class="action" href="">Xóa</a>
                        <a class="action" href="">Sửa</a>
                    </td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>Trần Thị B</td>
                    <td>Khoa học máy tính</td>
                    <td>
                        <a class="action" href="">Xóa</a>
                        <a class="action" href="">Sửa</a>
                    </td>
                </tr>
                <tr>
                    <td>3</td>
                    <td>Nguyễn Hoàng C</td>
                    <td>Khoa học vật liệu</td>
                    <td>
                        <a class="action" href="">Xóa</a>
                        <a class="action" href="">Sửa</a>
                    </td>
                </tr>
                <tr>
                    <td>4</td>
                    <td>Đinh Quang D</td>
                    <td>Khoa học vật liệu</td>
                    <td>
                        <a class="action" href="">Xóa</a>
                        <a class="action" href="">Sửa</a>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</body>

</html>

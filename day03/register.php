<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<style>
    body
    {
        font-family: Arial, Helvetica, sans-serif;
    }
    form
    {
        border: 2px solid #13489c;;
        padding: 50px;
    }
    .form
    {
        margin: 100px auto;
        box-sizing: border-box;
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
    }
    .item button
    {
        width: 150px;
        margin: 0 auto;
        background-color: rgb(61, 173, 85);
        color: white;
        height: 50px;
        border: 0;
        outline: 0;
        cursor: pointer;
    }
    .item input[type='input']
    {
        width: 150px;
    }
    .item select
    {
        width: 150px;
    }
    .item
    {
        margin-top: 50px;
       display: flex;
       gap: 50px;
       align-items: center;
    }
    .item label
    {
        display: block;
        min-width: 100px;
        padding: 10px;
        background: #1589f2;
        border: 2px solid blue;
    }
</style>
<?php
    $gender = [
        [
            'key' => 0,
            'name' => 'Nam'
        ],
        [
            'key' => 1,
            'name' => 'Nữ'
        ]
          
    ];
    $department = [
        [
            'key' => 'MAT',
            'name' => 'Khoa học máy tính'
        ],
        [
            'key' => 'KDL',
            'name' => 'Khoa hoc dữ liệu'
        ]
          
    ];
?>
<body>
    <div class="form">
        <form action="">
            <div class="item">
                <label for="">Họ và tên</label>
                <input type="text">
            </div>
            <div class="item">
                <label for="">Giới tính</label>
                <div>
                    <?php
                    for ($i = 0; $i < count($gender); $i++) {
                        echo "<input type='radio' name='gender' value='{$gender[$i]['key']}'>{$gender[$i]['name']} &nbsp &nbsp";
                    }
                    ?>
                </div>
            </div>
            <div class="item">
                <label for="">Khoa</label>
                <select name="" id="">
                <option value="">---Chọn phân khoa---</option>
                    <?php
                        foreach($department as $item)
                        {
                            echo "<option value='{$item['key']}'>{$item['name']}</option>";
                        }
                    ?>
                </select>
            </div>
            <div class="item">
                <button>Register</button>
            </div>
        </form>
    </div>
</body>
</html>
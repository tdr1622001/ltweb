<!DOCTYPE html>
<html lang="vn">
<head>
    <meta charset="UTF-8">
    <title>Register</title>
    <style>
        .center-text {
            text-align: center; 
        }
        table {
            width: 100%;
            border-collapse: collapse;
        }
        th, td {
            border: 1px solid #000; 
            padding: 10px;
        }
        th {
            background-color: #0096FF;
            color: white;
        }
        input[type="text"] {
            width: 100%; 
            border: none; 
        }
    </style>
</head>
<body>
    <fieldset style="width: 600px; height: 600px; border: #0096FF solid">
        <h1 class="center-text">Form đăng ký sinh viên</h1>
        <?php
        session_start();
        date_default_timezone_set('Asia/Ho_Chi_Minh');

        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $check = true;
            if (empty(handler($_POST["name"]))) {
                echo "<div style='color: red;'>Hãy nhập tên</div>";
                $check = false;
            }
            if (empty($_POST["gender"])) {
                echo "<div style='color: red;'>Hãy chọn giới tính</div>";
                $check = false;
            }
            if (empty($_POST["dob_day"]) || empty($_POST["dob_month"]) || empty($_POST["dob_year"])) {
                echo "<div style='color: red;'>Hãy chọn ngày, tháng và năm sinh</div>";
                $check = false;
            }
            if ($check) {
                
                $_SESSION['name'] = $_POST['name'];
                $_SESSION['gender'] = $_POST['gender'];
                $_SESSION['dob_day'] = $_POST['dob_day'];
                $_SESSION['dob_month'] = $_POST['dob_month'];
                $_SESSION['dob_year'] = $_POST['dob_year'];
                $_SESSION['city'] = $_POST['city'];
                $_SESSION['district'] = $_POST['district'];
                $_SESSION['other-info'] = $_POST['other-info'];

                header("location: regist_student.php");
                exit(); 
            }
        }

        function handler($data)
        {
            $data = stripslashes($data);
            $data = trim($data);
            return $data;
        }
        ?>
        <form style="margin: 50px 70px 0 50px" method="post">
            <table>
                <tr height="40px">
                    <td style="background-color: #0096FF; vertical-align: top; text-align: left; padding: 15px 15px">
                        <label style="color: white;">Họ và tên</label>
                    </td>
                    <td>
                        <input type="text" name="name" style="line-height: 32px; border-color: #0096FF">
                    </td>
                </tr>
                <tr height="40px">
                    <td style="background-color: #0096FF; vertical-align: top; text-align: left; padding: 15px 15px">
                        <label style="color: white;">Giới tính</label>
                    </td>
                    <td>
                        <?php
                        $genderArr = array("Nam", "Nữ");
                        foreach ($genderArr as $gender) {
                            echo "
                                <label class='container'>
                                    <input type='radio' value='$gender' name='gender'>$gender
                                </label>";
                        }
                        ?>
                    </td>
                </tr>

                <tr height="40px">
                    <td style="background-color: #0096FF; vertical-align: top; text-align: left; padding: 15px 15px">
                        <label style="color: white;">Ngày sinh</label>
                    </td>
                    <td>
                        <select name="dob_day" style="line-height: 32px; border-color: #0096FF;">
                            <option value="">Ngày</option>
                            <?php
                            for ($i = 1; $i <= 31; $i++) {
                                echo "<option value='$i'>$i</option>";
                            }
                            ?>
                        </select>
                        <select name="dob_month" style="line-height: 32px; border-color: #0096FF;">
                            <option value="">Tháng</option>
                            <?php
                            for ($i = 1; $i <= 12; $i++) {
                                echo "<option value='$i'>$i</option>";
                            }
                            ?>
                        </select>
                        <select name="dob_year" style="line-height: 32px; border-color: #0096FF;">
                            <option value="">Năm</option>
                            <?php
                            for ($i = date("Y"); $i >= 1900; $i--) {
                                echo "<option value='$i'>$i</option>";
                            }
                            ?>
                        </select>
                    </td>
                </tr>

                <tr height="40px">
                    <td style="background-color: #0096FF; vertical-align: top; text-align: left; padding: 15px 15px">
                        <label style="color: white;">Địa chỉ</label>
                    </td>
                    <td>
                        <select name="city" id="city" onchange="updateDistricts()" style="line-height: 32px; border-color: #0096FF;">
                            <option value="">Thành phố</option>
                            <option value="Hanoi">Hà Nội</option>
                            <option value="HCM">Hồ Chí Minh</option>
                        </select>
                        <select name="district" id="district" style="line-height: 32px; border-color: #0096FF;">
                            <option value="">Quận</option>
                        </select>
                    </td>
                </tr>

                <tr height="40px">
                    <td style="background-color: #0096FF; vertical-align: top; text-align: left; padding: 15px 15px">
                        <label style="color: white;">Thông tin khác</label>
                    </td>
                    <td>
                        <input type="text" name="other-info" style="line-height: 34px; border-color: #0096FF">
                    </td>
                </tr>
            </table>
            <button style="background-color: #0096FF; border-radius: 10px; width: 35%; height: 43px; border-width: 0; margin: 20px 130px; color: white;">Đăng Ký</button>
        </form>
    </fieldset>

    <script>
        function updateDistricts() {
            var citySelect = document.getElementById("city");
            var districtSelect = document.getElementById("district");

            while (districtSelect.options.length > 1) {
                districtSelect.remove(1);
            }

            if (citySelect.value === "Hanoi") {
                var hanoiDistricts = ["Hoàng Mai", "Thanh Trì", "Nam Từ Liêm", "Hà Đông", "Cầu Giấy"];
                for (var i = 0; i < hanoiDistricts.length; i++) {
                    var option = document.createElement("option");
                    option.text = hanoiDistricts[i];
                    option.value = hanoiDistricts[i];
                    districtSelect.add(option);
                }
            } else if (citySelect.value === "HCM") {
                var hcmDistricts = ["Quận 1", "Quận 2", "Quận 3", "Quận 7", "Quận 9"];
                for (var j = 0; j < hcmDistricts.length; j++) {
                    var option = document.createElement("option");
                    option.text = hcmDistricts[j];
                    option.value = hcmDistricts[j];
                    districtSelect.add(option);
                }
            }
        }

        updateDistricts();
    </script>
</body>
</html>

<!DOCTYPE html>
<html lang="vn">
<head>
    <meta charset="UTF-8">
    <title>Kết quả đăng ký</title>
    <style>
        .center-text {
            text-align: center; 
        }
        table {
            width: 100%;
            border-collapse: collapse;
        }
        th, td {
            border: 1px solid #000;
            padding: 10px;
        }
        th {
            background-color: #0096FF;
            color: white;
        }
    </style>
</head>
<body>
    <fieldset style="width: 600px; height: 600px; border: #FFFFFF solid">
        <?php
        session_start();
            echo "<table>";
            echo "<tr><th>Họ và tên</th><td>" . $_SESSION['name'] . "</td></tr>";
            echo "<tr><th>Giới tính</th><td>" . $_SESSION['gender'] . "</td></tr>";
            echo "<tr><th>Ngày sinh</th><td>" . $_SESSION['dob_day'] . "/" . $_SESSION['dob_month'] . "/" . $_SESSION['dob_year'] . "</td></tr>";
            echo "<tr><th>Địa chỉ</th><td>" . $_SESSION['district'] . ", " . $_SESSION['city'] . "</td></tr>";
            echo "<tr><th>Thông tin khác</th><td>" . $_SESSION['other-info'] . "</td></tr>";
            echo "</table>";
        ?>
    </fieldset>
</body>
</html>
